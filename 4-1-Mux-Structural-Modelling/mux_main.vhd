----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:54:47 09/26/2016 
-- Design Name: 
-- Module Name:    mux2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 4:1 MUX using Structural Modelling
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux2 is
    Port ( a : in  STD_LOGIC;
           b : in  STD_LOGIC;
           c : in  STD_LOGIC;
           d : in  STD_LOGIC;
           s0 : in  STD_LOGIC;
           s1 : in  STD_LOGIC;
           x : out  STD_LOGIC);
end mux2;

architecture Behavioral of mux2 is

component AND_GATE port(
	a, b, c : in STD_LOGIC;
	d : out STD_LOGIC );
end component;

component OR_GATE port(
	a, b, c, d : in STD_LOGIC;
	e : out STD_LOGIC);
end component; 

component NOT_GATE port(
	a : in STD_LOGIC;
	b : out STD_LOGIc);
end component;

signal t1, t2, t3, t4, s0_bar, s1_bar : STD_LOGIC;

begin

U1 : NOT_GATE port map (s0, s0_bar);
U2 : NOT_GATE port map (s1, s1_bar);
U3 : AND_GATE port map (a, s0_bar, s1_bar, t1);
U4 : AND_GATE port map (b, s0, s1_bar, t2);
U5 : AND_GATE port map (c, s1, s0_bar, t3);
U6 : AND_GATE port map (d, s1, s0, t4);
U7 : OR_GATE port map (t1, t2, t3, t4, x);

end Behavioral;

