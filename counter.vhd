----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:52:12 09/26/2016 
-- Design Name: 
-- Module Name:    main - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 4 bit synchronous mode controlled up/down counter (0-15)
--						1. active low clear(i.e. cr) & 2. active low preset(i.e. pr)
-- 					2. +ve edge triggering
-- 					3. mode 1(down counter) and mode 0(up counter) 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
    Port ( clk : in  STD_LOGIC;
           cr : in  STD_LOGIC;
           pr : in  STD_LOGIC;
           mode : in  STD_LOGIC;
           q : out  STD_LOGIC_VECTOR (3 downto 0));
end main;

architecture Behavioral of main is

signal temp : STD_LOGIC_VECTOR (3 downto 0);

begin

process(clk, cr, pr) begin 
	
	if(cr = '0') then 
		temp <= "0000";
		elsif(pr = '0') then 
			temp <= "1111";
			elsif(clk'event and clk = '1') then 
				if(mode = '1') then 
					temp <= temp - 1 ;
				else 
					temp <= temp + 1;
	
	end if;
	end if;
end process ;
q <= temp ;		
end Behavioral;

